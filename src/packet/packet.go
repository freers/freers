// Package packet handles creating communication packets outgoing from the server, as well
// as handling the parsing of packets coming from the client.
//
// This package contains the logic for handling communication between already
// connected clients and the server. As the login logic is very specific, it is
// not handled here. TODO: say where it's handled
package packet

import (
	"encoding/binary"
	"errors"
)

// Packeter is an interface for game packets to be compiled from easily
// constructed objects.
type Packeter interface {
	// Packet converts the data in the implementer of the
	// Packeter interface into a byte array, ready to be written to
	// the stream.
	Packet() ([]byte, error)
}

// BasePacket is the most basic packet struct, which can read specific
// data types from the Data member it has.
//
// Used as an embedded struct in structs that implement Packeter.
type BasePacket struct {
	Data []byte
}

// ReadByte reads a single byte from the Data array,
// moves the slice forward a single byte, and returns
// the byte that was just read.
func (b *BasePacket) ReadByte() (byte, error) {
	if len(b.Data) == 0 {
		return 0, errors.New("EOF")
	}
	readByte := b.Data[0]
	b.Data = b.Data[1:]
	return readByte, nil
}

// Move tries to move the Data slice by `count` bytes.
// If the buffer is shorter than `count`, it will move to the
// end of the array.
func (b *BasePacket) Move(count int) {
	if count > len(b.Data) {
		count = len(b.Data)
	}
	b.Data = b.Data[count:]
}

// ReadUint16 reads a single unsigned short from the Data array,
// moves the slice forward two bytes, and returns
// the short that was just read.
func (b *BasePacket) ReadUint16() (uint16, error) {
	if len(b.Data) < 2 {
		return 0, errors.New("Buffer is too short to be a uint16")
	}
	readShort := binary.BigEndian.Uint16(b.Data[:2])
	b.Data = b.Data[2:]
	return readShort, nil
}

// ReadInt64 reads a single int64 from the Data array,
// moves the slice forward a 8 bytes, and returns
// the number that was just read.
func (b *BasePacket) ReadInt64() (int64, error) {
	if len(b.Data) < 8 {
		return 0, errors.New("Buffer is too short to be an int64")
	}
	long := binary.LittleEndian.Uint64(b.Data[:8])
	b.Data = b.Data[8:]
	return int64(long), nil
}

// ReadInt32 reads a single int32 from the Data array,
// moves the slice forward a 4 bytes, and returns
// the number that was just read.
func (b *BasePacket) ReadInt32() (int32, error) {
	if len(b.Data) < 4 {
		return 0, errors.New("Buffer is too short to be an int32")
	}
	number := binary.LittleEndian.Uint32(b.Data[:4])
	b.Data = b.Data[4:]
	return int32(number), nil
}
