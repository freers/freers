// Package plogin contains the logic for creating and parsing login packets
package plogin

import (
	"encoding/binary"
	"fmt"
	"strings"

	"bitbucket.org/freers/freers/src/packet"
	"github.com/pkg/errors"
)

const (
	// MagicNumber is the beginning byte of the
	// login block. It is always 10.
	MagicNumber = 10
	// MinLength is the minumum length of a login
	// packet. This size excludes the username
	// and password.
	MinLength = 21
)

var (
	// ErrBadCredentials is an error returned when the client's username
	// and password are not separated by a line feed.
	ErrBadCredentials = errors.New("No linefeed in username/password string")
)

// Packet represents the login packet
type Packet struct {
	packet.BasePacket
	ClientISAACSeed int64
	ServerISAACSeed int64
	UserID          int
	Username        string
	Password        string // Nice
}

// Parse the given login packet into a Packet struct
func Parse(p []byte) (Packet, error) {
	// Define our packet with the base data
	base := Packet{
		BasePacket: packet.BasePacket{
			Data: p,
		},
	}
	if base.Data == nil {
		return Packet{}, errors.New("Packet is nil")
	}
	if len(base.Data) < MinLength {
		return Packet{}, errors.New("Packet is too short")
	}
	// Check that the block comes with the magic number
	if base.Data[0] != MagicNumber {
		return Packet{}, fmt.Errorf("Invalid magic number. Expected [%d], got [%d]", MagicNumber, base.Data[0])
	}
	var err error
	base.ClientISAACSeed, err = base.ReadInt64()
	if err != nil {
		return base, errors.Wrap(err, "base.ReadInt64(uid)")
	}
	base.ServerISAACSeed, err = base.ReadInt64()
	if err != nil {
		return base, errors.Wrap(err, "base.ReadInt64(server-uid)")
	}
	userID, err := base.ReadInt32()
	if err != nil {
		return base, errors.Wrap(err, "base.ReadInt32(user-id)")
	}
	base.UserID = int(userID)

	// Move one byte, because for some reason, this byte is
	// 12, and not a part of the actual username.
	base.Data = base.Data[1:]
	// Read the rest of the data as a string, separate username
	// and password using a line feed (\n)
	userAndPassword := strings.Split(string(base.Data), "\n")
	if len(userAndPassword) != 3 {
		return base, ErrBadCredentials
	}
	base.Username = userAndPassword[0]
	base.Password = userAndPassword[1]
	// Set data to nil, we're done with it. Let the GC take care of it.
	base.Data = nil
	// and return the packet

	return base, nil
}

func parseInt64(v []byte) int64 {
	long := binary.LittleEndian.Uint64(v)
	return int64(long)
}
