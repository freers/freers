package login

import (
	"encoding/binary"
	"math/rand"
	"time"

	"bitbucket.org/freers/freers/src/packet"
	"github.com/pkg/errors"
)

var (
	seeded = false
	// ErrOutOfDate is an error thrown by header() when the game client version is not
	// the same as the server's revision.
	ErrOutOfDate = errors.New("Game client is not on the same revision number")
)

const (
	expectedHeaderLength = 40
)

// HeaderResult contains all the collected data
// useful in the login process from the login header phase
type HeaderResult struct {
	ConnectionType byte
	ExpectedLength byte
	Client         ClientInfo
}

// ClientInfo contains the data about the client connecting to this server.
type ClientInfo struct {
	// LowMemeory tells us whether this user is a low memory client.
	LowMemory bool
}

// header handles the middle stage of the login process:
// * telling the client if we accept the connection
// * parsing the login header, checking if we support a specific connection type
// * reading the client revision number, CRC hash, comparing against our CRC hash
func (h Handler) header() (HeaderResult, error) {
	if err := h.acceptConnection(); err != nil {
		return HeaderResult{}, errors.Wrap(err, "acceptConnection")
	}
	hData, err := h.parseLoginHeader()
	if err != nil {
		return hData, errors.Wrap(err, "parseLoginHeader")
	}
	// Check if the expected length is at least longer than the rest of the header
	if hData.ExpectedLength <= expectedHeaderLength {
		// The rest of the data is shorter than the header, return an error
		return hData, errors.New("Expected length was shorter than the minimal length for pre-RSA data")
	}
	// set ExpectedLength to after what we expect from this packet
	gameData, err := h.readIntoBasePacket(expectedHeaderLength)
	if err != nil {
		return hData, errors.Wrap(err, "readIntoBasePacket")
	}

	// call to validate game client data that was just read
	client, err := h.getClientGameData(gameData)
	if err != nil {
		// Check if this was an ErrOutOfDate. If so, return it upwards.
		if err == ErrOutOfDate {
			return hData, ErrOutOfDate
		}
		// Otherwise, do a normal error return.
		return hData, errors.Wrap(err, "getClientGameData")
	}
	// Add info to header data
	hData.Client = client
	return hData, nil
}

// getClientGameData reads the given gameData base packet, containing the
// client revision, memory type, and cache CRCS.
func (h Handler) getClientGameData(gameData packet.BasePacket) (ClientInfo, error) {
	// Skip the first byte. It's the 255 magic number.
	gameData.Move(1)
	// Declare ClientInfo
	var client ClientInfo

	// Read the revision short
	revision, err := gameData.ReadUint16()
	if err != nil {
		return ClientInfo{}, errors.Wrap(err, "gameData.ReadUint16")
	}
	// Check if the revision is correct, if not return ErrOutOfDate
	if revision != uint16(h.config.Revision) {
		h.logger.Infof("Invalid revision, expected [%d], got [%d]", uint16(h.config.Revision), revision)
		return ClientInfo{}, ErrOutOfDate
	}

	// Move on to read the memory byte
	memory, err := gameData.ReadByte()
	if err != nil {
		return ClientInfo{}, errors.Wrap(err, "gameData.ReadByte")
	}
	if memory == MemoryHigh {
		client.LowMemory = false
	} else {
		client.LowMemory = true
	}
	// Read CRC bytes, compare them with the ones loaded from config.
	for index := 0; index < 9; index++ {
		expected := h.config.ExpectedCRCs[index]
		actual, err := gameData.ReadInt32()
		// Check for reading errors
		if err != nil {
			return client, errors.Wrap(err, "gameData.ReadInt32")
		}
		// Check if the hash is the same
		if expected != actual {
			// Bad CRC, drop the connection
			return client, errors.Wrap(err, "crc mismatch")
		}
	}
	return client, nil
}

// Reads the two login header bytes (connection type and length, respectively).
// Also checks if the connection type is supported. Returns both values.
func (h Handler) parseLoginHeader() (HeaderResult, error) {
	header, err := h.readIntoBasePacket(2)
	if err != nil {
		return HeaderResult{}, errors.Wrap(err, "readIntoBasePacket")
	}
	connectionType, err := header.ReadByte()
	if err != nil {
		return HeaderResult{}, errors.Wrap(err, "header.ReadByte")
	}
	// Check if the connection type is a new login. Relogs are not currently supported
	if connectionType != HeaderNewConnection {
		return HeaderResult{}, errors.New("Unsupported connection type")
	}
	expectedLength, err := header.ReadByte()
	if err != nil {
		return HeaderResult{}, errors.Wrap(err, "header.ReadByte")
	}

	return HeaderResult{
		ConnectionType: connectionType,
		ExpectedLength: expectedLength,
	}, nil
}

// Writes a []byte that tells the client to proceed with the login process.
// Also sends a unique session ID to the client.
func (h Handler) acceptConnection() error {
	// Prepare to write a long list of bytes for our next response.
	// First, 8 random bytes
	okResponse := []byte{0x16, 0x58, 0xf8, 0x8, 0x66, 0xa2, 0x16, 0x14} // Random bytes, should not be zero
	// Then, append StatusOK
	okResponse = append(okResponse, StatusOK)
	// And create a session ID
	h.SessionID = createSessionID()
	okResponse = append(okResponse, h.SessionID...)

	if err := h.write(okResponse...); err != nil {
		return errors.Wrap(err, "okresponse write")
	}
	return nil
}

func createSessionID() []byte {
	if !seeded {
		rand.Seed(time.Now().UnixNano())
		seeded = true
	}
	session := rand.Uint64()
	buffer := make([]byte, 8)
	binary.LittleEndian.PutUint64(buffer, session)
	return buffer
}
