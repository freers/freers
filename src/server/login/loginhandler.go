// Package login handles the code for the initial login process after
// a connection to the world is established.
package login

import (
	"crypto/rsa"
	"fmt"

	"bitbucket.org/freers/freers/src/jconf"
	"bitbucket.org/freers/freers/src/packet"
	"bitbucket.org/freers/freers/src/packet/plogin"
	"bitbucket.org/freers/freers/src/rsrsa"
	"bitbucket.org/freers/freers/src/server/player"
	"github.com/op/go-logging"
	"github.com/pkg/errors"
)

const (
	// StatusWaitRetryCount is the status byte returned in the login phase, telling the
	// client to wait 2000ms and try again, keeping track of the attempts made.
	StatusWaitRetryCount = iota - 1
	// StatusOK is the status byte returned upon the login protocol requested by
	// the client being accepted.
	StatusOK
	// StatusWaitRetry is the status byte returned in the login phase, telling the client
	// to wait 2000ms and try again.
	StatusWaitRetry
	// StatusSuccess is the status byte for a fully successful login.
	StatusSuccess
	// StatusInvalidCredentials is the status byte for the username or password being invalid.
	StatusInvalidCredentials
	// StatusDisabledAccount is the status byte for attempting to login on a disabled account.
	StatusDisabledAccount
	// StatusAlreadyLoggedIn is the status byte when the player is attempting to log in while
	// their account is already logged in.
	StatusAlreadyLoggedIn
	// StatusOutOfDate is the status byte telling the client that it's out of date for the current.
	// game version.
	StatusOutOfDate
	// StatusWorldFull is the status byte when the world is full of players and can't take any more.
	StatusWorldFull
	// StatusLoginServerOffline is the status byte for when the login server cannot take any new login requests.
	StatusLoginServerOffline
	// StatusIPAddressLimit is the status byte for when the server has had too many connection attempts from that IP Address.
	StatusIPAddressLimit
	// StatusBadSessionID is the status byte when the server could not complete a reconnection due to an invalid session ID.
	StatusBadSessionID
	// StatusRejected is the status byte when the server rejects the session. No more data available.
	StatusRejected
	// StatusMemberWorld is the status byte for a free player trying to log in to a member world.
	StatusMemberWorld
	// StatusFailedLogin is the status byte for when the server could not complete the login request.
	StatusFailedLogin
	// StatusUpdating is the status byte of the server telling the client that it is currently updating to a newer version.
	StatusUpdating
	// StatusPlayerRegistered is the status byte returned when the player is still registered
	// in the world, but may or may not be logged in. Regardless, the login cannot proceed.
	StatusPlayerRegistered
	// StatusLoginLimit is the status byte returned when the player has exceeded his current login attempt limit.
	StatusLoginLimit
	// StatusInMemberArea is the status byte when the user is trying to log in to a free world while standing in a member area.
	StatusInMemberArea
	// StatusInvalidLoginServer "Invalid loginserver requested. Please try using a different world."
	StatusInvalidLoginServer = 20
	// StatusProfileTransferring is the status byte when the user has just left a previous world and the server isn't done with their data.
	StatusProfileTransferring = 21
	// StatusNotImplemented is the status byte for when a client tries to do something that is not implemented
	StatusNotImplemented = 80
)

const (
	// HeaderNewConnection is the login header byte for establishing a new connection
	HeaderNewConnection = 16
	// HeaderReconnecting is the login header byte for reconnecting after a connection has been lost
	HeaderReconnecting = 18
)

const (
	// ProtocolLoginRequest is the protocol byte for a new login
	ProtocolLoginRequest = 14
)

const (
	// MemoryHigh indicates a high memory client
	MemoryHigh = iota
	// MemoryLow indicates a low memory client
	MemoryLow
)

// Handler handles the login operation for the server
type Handler struct {
	logger *logging.Logger
	user   *player.Player
	config jconf.Server
	key    *rsa.PrivateKey
	SessionID []byte
}

// New creates a new Login Handler.
func New(p *player.Player, conf jconf.Server, rsaKey *rsa.PrivateKey) Handler {
	return Handler{
		logger: logging.MustGetLogger("LoginHandler"),
		user:   p,
		config: conf,
		key:    rsaKey,
	}
}

func (h Handler) write(data ...byte) error {
	_, err := h.user.Connection().Write(data)
	return err
}

// Skip reading count number of bytes.
func (h Handler) skip(count int) {
	skipper := make([]byte, count)
	if _, err := h.user.Connection().Read(skipper); err != nil {
		h.logger.Debugf("Error skipping [%d] bytes: %s", count, err.Error())
	}
}

// CompleteSuccess is called to write to the client that the login was completed succesfully.
func (h Handler) CompleteSuccess() error {
	return h.write(StatusSuccess)
}

// CompleteInvalid is called to write to the client that the login process was completed,
// but the username or password were not recognized as valid.
func (h Handler) CompleteInvalid() error {
	return h.write(StatusInvalidCredentials)
}

// Login listens to the player's TCP connection and interacts with the client in an attempt to log them in.
// Upon successful login, returns the player login packet.
func (h Handler) Login() (plogin.Packet, error) {
	_, err := h.handshake()
	if err != nil {
		h.write(StatusFailedLogin)
		return plogin.Packet{}, errors.Wrap(err, "handshake")
	}
	headerResult, err := h.header() 
	if err != nil {
		// Check if the error is an ErrOutOfDate
		if err == ErrOutOfDate {
			// Tell the client he's out of date
			h.write(StatusOutOfDate)
		} else {
			// Otherwise, just a failed login byte
			h.write(StatusFailedLogin)
		}
		return plogin.Packet{}, errors.Wrap(err, "header")
	}
	// Set the player memory
	h.user.LowMemory = headerResult.Client.LowMemory

	// Get the RSA-decoded login block
	rsaResult, err := h.decodeRSA(int(headerResult.ExpectedLength))
	if err != nil {
		return rsaResult, errors.Wrap(err, "decodeRSA")
	}
	return rsaResult, nil
}

// readIntoBasePacket reads x bytes, where x <= length from the
// TCP connection, and returns the collected data in the form of a BasePacket
// struct for easy reading.
func (h Handler) readIntoBasePacket(length int) (packet.BasePacket, error) {
	data := make([]byte, length)
	dataLength, err := h.user.Connection().Read(data)
	if err != nil {
		return packet.BasePacket{}, errors.Wrapf(err, "connection.Read(%d)", length)
	}
	return packet.BasePacket{
		Data: data[:dataLength],
	}, nil
}

// getProtocol reads the initial protocol byte
func (h Handler) getProtocol() (byte, error) {
	// Read a one byte packet
	buffer, err := h.readIntoBasePacket(1)
	if err != nil {
		return 0, errors.Wrap(err, "readIntoBasePacket")
	}
	protocol, err := buffer.ReadByte()
	if err != nil {
		return 0, errors.Wrap(err, "buffer.ReadByte")
	}
	// Check if the protocol is supported
	if protocol != ProtocolLoginRequest {
		// Currently, login request is the only login protocol supported.
		return protocol, errors.New("protocol is not supported, please use 14")
	}
	return protocol, nil
}

// Handle the connection and attempt to log the client in.
// This function requires the server's RSA key to decode the client's
// RSA-Encrypted messages.
func (h Handler) decodeRSA(expectedLength int) (plogin.Packet, error) {
	// Get the RSA-encrypted login block. The first byte being the length
	// of the entire block.
	rsaBlock, err := h.readIntoBasePacket(expectedLength)
	if err != nil {
		return plogin.Packet{}, errors.Wrap(err, "readIntoBasePacket")
	}
	// Read the length byte
	length, err := rsaBlock.ReadByte()
	if err != nil {
		return plogin.Packet{}, errors.Wrap(err, "readByte")
	}
	// Check if length is at least the length of the remaining data
	if int(length) < len(rsaBlock.Data) {
		// It's not.
		return plogin.Packet{}, fmt.Errorf("RSA block length mismatch. Expected: [%d], got [%d]", length, len(rsaBlock.Data))
	}

	loginBytes := rsrsa.Decrypt(h.key, rsaBlock.Data[:length])
	loginPacket, err := plogin.Parse(loginBytes)
	if err != nil {
		return plogin.Packet{}, errors.Wrap(err, "plogin.Parse")
	}

	return loginPacket, nil
}

