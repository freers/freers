package login

import (
	"github.com/pkg/errors"
)

// HandshakeResult returns all the collected data
// useful in the login process from the handshake phase
type HandshakeResult struct {
	Protocol byte
}

func (h Handler) handshake() (HandshakeResult, error) {
	protocol, err := h.getProtocol()
	if err != nil {
		// Invalid protocol or failed reading connection. Either way, drop the client.
		// Write a rejected response. Ignore any errors, since I don't care if the
		// stream is closed.
		h.write(StatusRejected)
		return HandshakeResult{}, errors.Wrap(err, "getProtocol")
	}
	// Ignore the next byte. This byte is supposed to be derived from the username,
	// and is to be used to determine a login server. But we don't do that.
	h.skip(1)

	return HandshakeResult{
		Protocol: protocol,
	}, nil
}
