// Package player contains the definitions and logic for dealing with player clients.
package player

import "net"

// Player contains the connection and various useful information of the player.
type Player struct {
	conn net.Conn
	// LowMemory defines whether the user is a low or high memory client
	LowMemory bool
}

// New creates a new Player wrapper with the given c TCP connection.
func New(c net.Conn) *Player {
	return &Player{
		conn: c,
	}
}

// Connection returns the TCP connection interface for the player
func (p *Player) Connection() net.Conn {
	return p.conn
}
