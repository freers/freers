package server

import (
	"crypto/rsa"
	"fmt"
	"net"
	"sync"

	"bitbucket.org/freers/freers/src/jconf"
	"bitbucket.org/freers/freers/src/server/login"
	"bitbucket.org/freers/freers/src/server/player"
	"github.com/op/go-logging"
	"github.com/pkg/errors"
)

// World represents an entire server world.
type World struct {
	tcp    net.Listener
	addr   string
	log    *logging.Logger
	number int
	lock   *sync.Mutex
	quit   bool
	// quitCh is the equivalent of quit, just sent via channel for goroutines
	// which are entirely dependent on channels
	quitCh chan bool
	rsaKey *rsa.PrivateKey
	config jconf.Server
}

// New creates a new instance of a World
func New(wNumber int, key *rsa.PrivateKey) *World {
	return &World{
		log:    logging.MustGetLogger(fmt.Sprintf("World [%d]", wNumber)),
		number: wNumber,
		lock:   &sync.Mutex{},
		quitCh: make(chan bool, 8),
		rsaKey: key,
	}
}

// Start the world by beginning to listen on TCP.
//
// This function will not block, but will return an error if it fails initializing a TCP listener.
func (w *World) Start() error {
	// First, read the server config
	conf := jconf.Server{}
	if err := jconf.Read(jconf.ConfigServer, &conf); err != nil {
		return errors.Wrap(err, "jconf.Read")
	}
	// And set it in world
	w.config = conf

	// Define err here, because we cannot redeclare w.tcp
	var err error
	w.log.Infof("Starting server on [%s]; running revision [%d]", w.config.Address, w.config.Revision)
	w.tcp, err = net.Listen("tcp", w.config.Address)
	if err != nil {
		return errors.Wrap(err, "net.Listen")
	}
	// Start accepting connections in another goroutine
	go w.listenAndServe()

	// And report that creating the TCP channel has resulted in no errors
	return nil
}

func (w *World) shouldQuit() bool {
	w.lock.Lock()
	defer w.lock.Unlock()
	w.quitCh <- true
	return w.quit
}

// Stop the world from accepting connections.
func (w *World) Stop() error {
	// Thread-safely set w.quit to true, so that the listenAndServe loop
	// can read that it's time to stop after it gets an error from tcp.Accept
	// since the stream is closed.
	w.lock.Lock()
	defer w.lock.Unlock()
	w.quit = true

	return w.tcp.Close()
}

func (w *World) listenAndServe() {
	// Run the loop until shouldQuit return true, which happens when the server is told
	// to stop this world.
	for !w.shouldQuit() {
		conn, err := w.tcp.Accept()
		if err != nil {
			w.log.Errorf("Error in tcp.Accept: %s", err.Error())
			continue
		}
		// Write a log about the connection
		w.log.Infof("Got a connection from [%s]", conn.RemoteAddr().String())
		// Send the connection to the connection handler channel
		go w.login(player.New(conn))
	}
}

func (w *World) login(p *player.Player) {
	loginHandler := login.New(p, w.config, w.rsaKey)
	user, err := loginHandler.Login()
	if err != nil {
		w.log.Infof("Error in logging in user from [%s]: [%s]", p.Connection().RemoteAddr().String(), err.Error())
		p.Connection().Close()
		return
	}
	// Just log them in without questions for now
	if err := loginHandler.CompleteSuccess(); err != nil {
		w.log.Errorf("Failed to say login is ok :( [%s]", err.Error())
		p.Connection().Close()
		return
	}
	w.log.Infof("Successfully logged in [%s]", user.Username)
}
