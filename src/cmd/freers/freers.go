package main

import (
	"fmt"

	"bitbucket.org/freers/freers/src/rsrsa"
	"bitbucket.org/freers/freers/src/server"
)

func main() {
	// Load the RSA key from the environment, panic if this fails
	key, err := rsrsa.LoadRSA()
	if err != nil {
		panic("Failed loading RSA key: " + err.Error())
	}

	world1 := server.New(1, key)
	if err := world1.Start(); err != nil {
		panic(fmt.Sprintf("Failed starting server: %s", err.Error()))
	}

	// Block this goroutine indefinitely
	select {}
}
