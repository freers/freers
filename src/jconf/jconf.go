// Package jconf contains the logic for loading JSON configuration data
// for the FreeRS server.
package jconf

import (
	"encoding/json"
	"io/ioutil"
	"path/filepath"

	"github.com/pkg/errors"
)

// Server contains the server configuration data
type Server struct {
	// Revision number of the server
	Revision int
	// Address for the TCP server to listen on
	Address string
	// ExpectedCRCs is the expected CRC value of the client's cache
	ExpectedCRCs []int32
}

// ConfigName defines a type called ConfigName as alias to string
// so that Read can only accept strings specifically
// typed as ConfigNames
type ConfigName string

const (
	configDir = "config"
	// ConfigServer contains the server configuration data
	ConfigServer ConfigName = "server.json"
)

// Read the given config file into v. v MUST be a pointer to the appropriate
// config struct. The data will be read into that struct.
func Read(config ConfigName, v interface{}) error {
	// Get the path to the config file
	path := filepath.Join(".", configDir, string(config))
	// Read the actual file into a []byte
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return errors.Wrap(err, "ioutil.ReadFile")
	}
	// Deserialize the JSON config
	if err := json.Unmarshal(data, v); err != nil {
		return errors.Wrap(err, "json.Unmarshal")
	}
	return nil
}
