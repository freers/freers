// Package rsrsa contains tools to load the RSA key from the environment, and use it to encrypt/decrypt client/server
// communication
package rsrsa

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/hex"
	"encoding/pem"
	"math/big"
	"os"

	"github.com/pkg/errors"
)

// LoadRSA loads the RSA key from the environment variables
func LoadRSA() (*rsa.PrivateKey, error) {
	key, err := hex.DecodeString(os.Getenv("RSA_HEX"))
	if err != nil {
		return nil, errors.Wrap(err, "hex.DecodeString")
	}
	passcode := os.Getenv("RSA_PASSCODE")
	// Get the Private Key PEM block
	privPem, _ := pem.Decode(key)
	privatePemBytes := []byte{}
	if privPem.Type != "RSA PRIVATE KEY" {
		return nil, errors.New("RSA_HEX is not a valid RSA Private Key")
	}
	privatePemBytes, err = x509.DecryptPEMBlock(privPem, []byte(passcode))
	if err != nil {
		return nil, errors.New("RSA_PASSCODE is incorrect for this RSA key")
	}
	// Declare parsedKey as an empty interface, since x509 doesn't care if it's a *rsa.PrivateKey object
	var parsedKey interface{}
	// Try parsing as PKCS1 first. If that fails, go for PKCS8. If both fail, give up.
	if parsedKey, err = x509.ParsePKCS1PrivateKey(privatePemBytes); err != nil {
		if parsedKey, err = x509.ParsePKCS8PrivateKey(privatePemBytes); err != nil { // note this returns type `interface{}`
			return nil, errors.Wrap(err, "x509.Parse...")
		}
	}
	// Typecast parsedKey into an RSA Private Key
	privateKey, ok := parsedKey.(*rsa.PrivateKey)
	// Check if this was valid
	if !ok {
		return nil, errors.New("RSA_HEX parsing did not produce a valid RSA Key")
	}
	return privateKey, nil
}

// NewRSAHex creates a new RSA key of the desired length and returns the key as
// a hex-encoded PEM block.
func NewRSAHex(length int, password string) (string, error) {
	k, err := rsa.GenerateKey(rand.Reader, length)
	if err != nil {
		return "", errors.Wrap(err, "generateKey")
	}
	b := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(k),
	}
	encryptedBlock, err := x509.EncryptPEMBlock(rand.Reader, b.Type, b.Bytes, []byte(password), x509.PEMCipherAES256)
	if err != nil {
		return "", errors.Wrap(err, "x509.EncryptPEMBlock")
	}
	keyBytes := pem.EncodeToMemory(encryptedBlock)
	return hex.EncodeToString(keyBytes), nil
}

// Decrypt the given data using the provided RSA key using no padding
func Decrypt(key *rsa.PrivateKey, data []byte) []byte {
	dataBigInt := new(big.Int).SetBytes(data)
	return dataBigInt.Exp(dataBigInt, key.D, key.N).Bytes()
}
