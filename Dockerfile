FROM golang:1.11

WORKDIR /freers
COPY . .

RUN go get && \
	go install -race ./src/cmd/freers

CMD ["freers"]